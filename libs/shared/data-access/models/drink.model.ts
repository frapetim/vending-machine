export interface BaseState {
  stock: number;
  cost: number;
}
export interface DrinkBase {
  name: string;
  price: string;
}

export interface Purchase {
  quantity: number;
  amount: number;
}
