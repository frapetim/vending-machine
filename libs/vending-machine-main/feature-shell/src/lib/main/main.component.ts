import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Purchase } from '@shared/data-access';
import { VendingMachineFacade } from '@vending-machine/data-access';

@Component({
  selector: 'vend-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent {
  DEFAULT_RESTOCK_QUANTITY = 10;

  constructor(public vendingMachineFacade: VendingMachineFacade) {}

  /** Subit a new purchase to the Store */
  newOrder(value: Purchase) {
    this.vendingMachineFacade.newOrder(value);
  }

  /** Restock for a custom amount or based on the default value */
  restock() {
    this.vendingMachineFacade.restock();
  }
}
