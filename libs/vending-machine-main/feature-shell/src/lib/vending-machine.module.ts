import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { VendingMachineMainUiModule } from '@vending-machine/ui';
import { VendingMachineMainDataAccessModule } from '@vending-machine/data-access';
import { MainComponent } from './main/main.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: MainComponent }]),
    VendingMachineMainUiModule,
    VendingMachineMainDataAccessModule,
  ],
  declarations: [MainComponent],
})
export class VendingMachineFeatureShellModule {}
