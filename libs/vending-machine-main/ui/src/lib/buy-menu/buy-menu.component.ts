import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MESSAGE_TEMPLATES } from 'apps/vending-machine/src/assets/config';

export interface PeriodicElement {
  name: string;
  position: number;
}

/** Custom Validator for amount/quantity combined */
export function purchaseValidator() {
  return (controls: AbstractControl) => {
    let amount = controls.get('amount');
    let quantity = controls.get('quantity');

    /** Check that the amount is sufficent to buy the requested quantity */
    if (amount.value / (1.2 * quantity.value) < 1) {
      return amount.setErrors({
        fundsError: MESSAGE_TEMPLATES.fundsError,
      });
    }
    return null;
  };
}

@Component({
  selector: 'vend-buy-menu',
  templateUrl: './buy-menu.component.html',
  styleUrls: ['./buy-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BuyMenuComponent implements OnChanges {
  public form: FormGroup;

  private snackbarDuration = 5000;

  @Input() restocked: boolean;
  @Input() errorMessage: string;

  @Output() purchase = new EventEmitter<{ amount: string; quantity: number }>();

  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar) {
    this.form = this.fb.group(
      {
        amount: [null, [Validators.required, Validators.nullValidator]],
        quantity: [1, Validators.required],
      },
      {
        validator: purchaseValidator(),
      } as AbstractControlOptions
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.restocked && this.restocked) {
      this.displaySnackBar(MESSAGE_TEMPLATES.resupplySuccess);
    }
    if (changes.errorMessage) {
      this.displaySnackBar(this.errorMessage);
    }
  }

  displaySnackBar(errorMessage: string) {
    errorMessage &&
      this._snackBar.open(errorMessage, 'Error', {
        duration: this.snackbarDuration,
        verticalPosition: 'top',
      });
  }

  onSubmit() {
    this.form.valid
      ? this.submitFrom()
      : this.displaySnackBar(this.form.get('amount').errors.fundsError);
  }

  submitFrom() {
    this.purchase.emit(this.form.value);
    this.form.reset({ amount: null, quantity: 1 });
  }
}
