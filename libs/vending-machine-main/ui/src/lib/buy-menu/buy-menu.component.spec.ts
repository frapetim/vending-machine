import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyMenuComponent } from './buy-menu.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

describe('BuyMenuComponent', () => {
  let component: BuyMenuComponent;
  let fixture: ComponentFixture<BuyMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuyMenuComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSnackBarModule,
        MatInputModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
