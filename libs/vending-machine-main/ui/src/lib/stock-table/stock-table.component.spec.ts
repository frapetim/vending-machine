import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTableComponent } from './stock-table.component';
import { MatTableModule } from '@angular/material/table';

describe('StockTableComponent', () => {
  let component: StockTableComponent;
  let fixture: ComponentFixture<StockTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StockTableComponent],
      imports: [MatTableModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
