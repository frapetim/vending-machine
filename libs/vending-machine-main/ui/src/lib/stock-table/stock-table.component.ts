import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DrinkBase } from '@shared/data-access';
@Component({
  selector: 'vend-stock-table',
  templateUrl: './stock-table.component.html',
  styleUrls: ['./stock-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StockTableComponent {
  displayedColumns: string[] = ['name', 'price'];

  @Input() drinkList: DrinkBase[];
}
