import {
  Component,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  Input,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DrinkBase, Purchase } from '@shared/data-access';

@Component({
  selector: 'vend-vending-machine-ui',
  templateUrl: './vending-machine-ui.component.html',
  styleUrls: ['./vending-machine-ui.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VendingMachineUiComponent {
  public hideBuyMenu = true;
  public error: string;

  @Input() dataSource: DrinkBase[];
  @Input() currentStock: number;
  @Input() change: number = 0;
  @Input() purchaseError: string;
  @Input() restocked: boolean;

  @Output() purchase = new EventEmitter<Purchase>();
  @Output() restock = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  newOrder(value: Purchase) {
    this.purchase.emit(value);

    this.hideBuyMenu = true;
  }

  /** Add 10 units to the curretn vendinag machine stock */
  restockVendingMachine() {
    this.restock.emit();
  }
}
