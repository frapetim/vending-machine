import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyMenuComponent } from '../buy-menu/buy-menu.component';
import { StockTableComponent } from '../stock-table/stock-table.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { VendingMachineUiComponent } from './vending-machine-ui.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTableModule } from '@angular/material/table';

describe('VendingMachineUiComponent', () => {
  let component: VendingMachineUiComponent;
  let fixture: ComponentFixture<VendingMachineUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        VendingMachineUiComponent,
        StockTableComponent,
        BuyMenuComponent,
      ],
      imports: [
        MatDialogModule,
        MatCardModule,
        MatMenuModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatInputModule,
        MatBadgeModule,
        MatTableModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendingMachineUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
