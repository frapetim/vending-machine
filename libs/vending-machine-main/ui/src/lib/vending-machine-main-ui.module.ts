import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { VendingMachineUiComponent } from './vending-machine-ui/vending-machine-ui.component';
import { BuyMenuComponent } from './buy-menu/buy-menu.component';
import { StockTableComponent } from './stock-table/stock-table.component';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const PUBLIC_COMPONENTS = [VendingMachineUiComponent];

const PRIVATE_COMPONENTS = [BuyMenuComponent, StockTableComponent];

/** Declare here all the Material Component needed */
const MATERIAL_COMPONENTS = [
  MatCardModule,
  MatButtonModule,
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonToggleModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatFormFieldModule,
  MatTableModule,
  MatSnackBarModule,
];

@NgModule({
  imports: [
    MATERIAL_COMPONENTS,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [PUBLIC_COMPONENTS, PRIVATE_COMPONENTS],
  exports: [PUBLIC_COMPONENTS],
})
export class VendingMachineMainUiModule {}
