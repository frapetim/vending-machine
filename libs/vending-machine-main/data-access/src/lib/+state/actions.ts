import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { DrinkBase, Purchase } from '@shared/data-access';

export const purchaseOrder = createAction(
  '[VendingMachine] Purchase',
  props<{ order: Purchase }>()
);

export const purchaseSuccess = createAction(
  '[VendingMachine] Purchase Success',
  props<{ change: number; quantity: number }>()
);

export const purchaseFailure = createAction(
  '[VendingMachine] Purchase Failure',
  props<{ error: HttpErrorResponse | Error }>()
);

export const restock = createAction('[VendingMachine] Restock');

export const restockSuccess = createAction(
  '[VendingMachine] Restock Success',
  props<{ updatedStock: DrinkBase[] }>()
);
export const restockFailure = createAction(
  '[VendingMachine] Restock Failure',
  props<{ error: HttpErrorResponse | Error }>()
);
