import { DrinkBase } from '@shared/data-access';
import { initialState } from './reducer';
import { vendingMachineQuery } from './selectors';

// No complex selectors with business logic
describe('Selectors', () => {
  it('should select the stock', () => {
    const stock: DrinkBase[] = [{ name: 'fake-cola', price: '1.2' }];
    const state = { ...initialState, stock };

    const result = vendingMachineQuery.selectStock.projector(
      state,
      state.stock
    );
    expect(result).toBe(stock.length);
  });
});
