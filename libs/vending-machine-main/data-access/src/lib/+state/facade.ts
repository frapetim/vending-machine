import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Purchase } from '@shared/data-access';
import { purchaseOrder, restock } from './actions';
import { State } from './reducer';
import { vendingMachineQuery } from './selectors';

@Injectable()
export class VendingMachineFacade {
  currentStockList$ = this.store.select(vendingMachineQuery.selectStockList);
  currentStock$ = this.store.select(vendingMachineQuery.selectStock);
  change$ = this.store.select(vendingMachineQuery.selectChange);
  purchaseError$ = this.store.select(vendingMachineQuery.getPurchaseError);
  restocked$ = this.store.select(vendingMachineQuery.selectRestocked);

  constructor(private store: Store<State>) {}

  newOrder(order: Purchase) {
    this.store.dispatch(purchaseOrder({ order }));
  }
  restock() {
    this.store.dispatch(restock());
  }
}
