import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, VENDINGMACHINE_FEATURE_KEY } from './reducer';

const getState = createFeatureSelector<State>(VENDINGMACHINE_FEATURE_KEY);

const getPurchaseError = createSelector(
  getState,
  (state: State) => state.purchaseError
);

const selectCost = createSelector(getState, (state: State) => state.cost);

const selectStockList = createSelector(getState, (state: State) => state.stock);

const selectStock = createSelector(
  getState,
  (state: State) => state.stock.length
);

const selectChange = createSelector(getState, (state: State) => state.change);

const selectPurchasing = createSelector(
  getState,
  (state: State) => state.purchasing
);

const selectPurchaseSuccess = createSelector(
  getState,
  (state: State) => state.purchaseSuccess
);

const selectRestocked = createSelector(
  getState,
  (state: State) => state.restocked
);

export const vendingMachineQuery = {
  selectCost,
  selectStock,
  selectChange,
  selectRestocked,
  selectStockList,
  getPurchaseError,
  selectPurchasing,
  selectPurchaseSuccess,
};
