import { Action, createReducer, on } from '@ngrx/store';
import { DrinkBase } from '@shared/data-access';
import {
  purchaseFailure,
  purchaseOrder,
  purchaseSuccess,
  restockSuccess,
} from './actions';

export const VENDINGMACHINE_FEATURE_KEY = 'vending-machine';

export interface State {
  cost: number;
  stock: DrinkBase[];
  purchasing?: boolean;
  purchaseSuccess?: boolean;
  purchaseError?: Error;
  change?: number;
  restocked: boolean;
}

export const initialState: State = {
  cost: 1.2,
  stock: [{ name: 'fake-cola', price: '$1.2' }],
  restocked: false,
};

const vendingMachineReducer = createReducer(
  initialState,
  on(purchaseOrder, (state) => ({
    ...state,
    purchasing: true,
    purchaseSuccess: false,
    purchaseError: undefined,
    change: undefined,
  })),
  on(purchaseSuccess, (state, { change, quantity }) => ({
    ...state,
    change,
    purchaseSuccess: true,
    purchasing: false,
    purchaseError: undefined,
    stock: state.stock.slice(quantity),
    restocked: false,
  })),
  on(purchaseFailure, (state, { error }) => ({
    ...state,
    change: undefined,
    purchaseSuccess: false,
    purchasing: false,
    purchaseError: error,
  })),
  on(restockSuccess, (state, { updatedStock }) => {
    return {
      ...state,
      stock: state.stock.concat(updatedStock),
      restocked: true,
      puchasing: false,
      change: undefined,
    };
  })
);

export function reducer(state: State, action: Action) {
  return vendingMachineReducer(state, action);
}
