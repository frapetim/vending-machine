import { DrinkBase } from '@shared/data-access';
import { purchaseSuccess, restock } from './actions';
import { initialState, reducer } from './reducer';

describe('VendingMachineReducer', () => {
  describe('PurchaseOrderSuccess', () => {
    it('should update stock after a purchase', () => {
      const change = 0.8;
      const action = purchaseSuccess({ change, quantity: 1 });
      const expectedState = {
        change,
        purchaseSuccess: true,
        stock: [],
        purchaseError: undefined,
        purchasing: false,
        cost: 1.2,
        restocked: false,
      };

      const state = reducer(initialState, action);

      expect(state).toEqual(expectedState);
    });
  });
});
