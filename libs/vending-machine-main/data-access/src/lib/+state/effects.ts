import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { DrinkBase } from '@shared/data-access';
import { of } from 'rxjs';
import { catchError, concatMap, map, withLatestFrom } from 'rxjs/operators';
import { VendingMachineService } from '../vending-machine.service';
import {
  restock,
  restockSuccess,
  restockFailure,
  purchaseOrder,
  purchaseSuccess,
  purchaseFailure,
} from './actions';
import { State } from './reducer';
import { vendingMachineQuery } from './selectors';

@Injectable()
export class VendingMachineEffects {
  restock$ = createEffect(() =>
    this.actions$.pipe(
      ofType(restock),
      concatMap(() =>
        this.service.genrateDefultRestockList().pipe(
          map((stock: DrinkBase[]) => restockSuccess({ updatedStock: stock })),
          catchError((error) => of(restockFailure({ error })))
        )
      )
    )
  );

  order$ = createEffect(() =>
    this.actions$.pipe(
      ofType(purchaseOrder),
      withLatestFrom(
        this.store.pipe(select(vendingMachineQuery.selectCost)),
        this.store.pipe(
          select(
            vendingMachineQuery.selectStock // This is for mock only, we retrive the current stock form the store, in a real world scenario the stock availability is dictated by the backend
          )
        )
      ),
      concatMap(([{ order }, cost, stock]) =>
        this.service.purchase(order, cost, stock).pipe(
          map((change) =>
            purchaseSuccess({
              change: Number(change.toFixed(2)),
              quantity: order.quantity,
            })
          ),
          catchError((error) => of(purchaseFailure({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private service: VendingMachineService
  ) {}
}
