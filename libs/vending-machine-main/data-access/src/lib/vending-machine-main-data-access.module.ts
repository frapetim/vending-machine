import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer, VENDINGMACHINE_FEATURE_KEY } from './+state/reducer';
import { VendingMachineEffects } from './+state/effects';
import { VendingMachineService } from './vending-machine.service';
import { VendingMachineFacade } from './+state/facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(VENDINGMACHINE_FEATURE_KEY, reducer),
    EffectsModule.forFeature([VendingMachineEffects]),
  ],
  providers: [VendingMachineService, VendingMachineFacade],
})
export class VendingMachineMainDataAccessModule {}
