import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Purchase, DrinkBase } from '@shared/data-access';
import { DEFAULT_RESTOCK_DRINKS } from 'apps/vending-machine/src/assets/config';
import { MESSAGE_TEMPLATES } from 'apps/vending-machine/src/assets/config';
@Injectable()
export class VendingMachineService {
  /** Return the change or an error */
  purchase(
    order: Purchase,
    cost: number,
    availableStock: number
  ): Observable<number> {
    if (availableStock) {
      return of(order.amount - order.quantity * cost);
    } else {
      return throwError(MESSAGE_TEMPLATES.stockError);
    }
  }

  genrateDefultRestockList(): Observable<DrinkBase[]> {
    /** Mock for fetch 10 items from a backend call */
    const serverFetchMock = of(
      [...Array(DEFAULT_RESTOCK_DRINKS).keys()].map((_) => ({
        name: 'fake-cola',
        price: '$1.2',
      }))
    );

    return serverFetchMock;
  }
}
