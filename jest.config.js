module.exports = {
  projects: [
    '<rootDir>/apps/vending-machine',
    // '<rootDir>/libs/vending-machine-main',
    '<rootDir>/libs/vending-machine-main/feature-shell',
    '<rootDir>/libs/vending-machine-main/ui',
    '<rootDir>/libs/vending-machine-main/data-access',
    // '<rootDir>/libs/shared/data-access',
  ],
};
