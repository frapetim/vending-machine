/** Insert here all Global Configurations */

export const DEFAULT_RESTOCK_DRINKS: number = 10;

export const MESSAGE_TEMPLATES = {
  stockError: 'Out of stock',
  fundsError: 'Insufficent Money',
  resupplySuccess: 'Resupplied with 10 cans',
};
