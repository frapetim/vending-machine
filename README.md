# Vending Machine App

This Angular application has been created using the latest Nx version.

I have attempted to set up and structure the project as I would for an enterprise-level application and followed as much as possible all the guidelines and provided by the Nx team.

## Getting Started

Unzip the project into a folder or git clone the repository:

https://bitbucket.org/frapetim/vending-machine/src/master/
To run the application navigate to the root level of the application and type on the comand line:

    nx serve

## Prerequisites

Once the application has been clone it is necessary to install all dependencies.
From the comand line ensure you are in the root of the project and type the comand:

    npm install

## Running the App

To run the app from the comand line ensure you are in the root of the project and type the comand:

    nx serve

## Running the tests

To run the test suite from the comand line ensure you are in the root of the project and type the comand:

    npm run test-all

# Additional Notes

The folder structure follows the Nx guidelines, using a data access layer where all the business logic resides, a UI layer with presentational only components accepting Input and Output, and a feature shell to connect the two. All components use onPush change detection to avoid using the standard Angular change detection that runs on every change.

For more information see the airline app example in the official guide:
https://go.nrwl.io/angular-enterprise-monorepo-patterns-new-book

For the Store, a global NgRx store has been set up to mimic a real application. I have used the Facade pattern to interface between the feature-shell component and the UI.
I have decided not to use the newly implemented Component Store from NgRx, although I could have accomplished the same result with less boilerplate code.

Unit Testing was done with Jest that comes already set up with Nx.
The current unit testing is minimal and not adequate for a production-ready app. With more time available, I would focus on testing the business logic to an acceptable level.

No E2E testing was done on this application.

For the UI I have used the standard Angular Material library, for a real-world application, I would have probably opted for using the CDK with custom styling.

For git branching model a simple Master and Develop was used
